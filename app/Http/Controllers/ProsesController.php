<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    //enkripsi transformasi
    public function proses(Request $request)
    {
        $metode = $request->input('metode');
        $key = (int) $request->input('key');
        $text = $request->input('text');

        if ($metode == '1') {
            // mendeklarasikan matriks
            $matriks = [];
            $baris = 0;
            $kolom = 0;
            $dividen = ceil(strlen($text) / $key);

            // memeriksa panjang text
            $panjang = strlen($text);
            if ($panjang % $key != 0) {
                $panjang = $panjang + ($panjang % $key) + 1;
                $dividen = ceil($panjang / $key);
            }

            // menyusun matriks 
            for ($idx = 0; $idx < $panjang; $idx++) {
                $matriks[$baris][$kolom] = isset($text[$idx])? $text[$idx] : 'ks';

                if ($kolom == $key - 1) {
                    $kolom = 0;
                    $baris = $baris + 1;
                } else { 
                    $kolom = $kolom + 1;
                }
            }

            // dd($matriks, $panjang);

            // mereset baris dan kolom
            $baris = 0;
            $kolom = 0;
            
            // mentranspose matriks
            $hasil = '';
            for ($idx = 0; $idx < $panjang; $idx++) {
                if ($matriks[$kolom][$baris] == 'ks') {
                    $hasil = $hasil . '';
                } else {
                    $hasil = $hasil . $matriks[$kolom][$baris];
                }

                if ($kolom == $dividen - 1) {
                    $kolom = 0;
                    $baris = $baris + 1;
                } else { 
                    $kolom = $kolom + 1;
                }
            }
            
            return view('dashboard', [
                'hasil' => $hasil,
                'metode' => $metode,
                'key' => $key,
                'text' => $text
            ]);
        }

        //dekripsi transformasi
        elseif ($metode == '2') {
            // mendeklarasikan matriks
            $matriks = [];
            $baris = 0;
            $kolom = 0;

            // memeriksa panjang text
            $panjang = strlen($text);
            $sisa = 0;
            $dividen = ceil($panjang / $key);
            
            if ($panjang % $key != 0) {
                $sisa = $panjang % $key;
                $dividen = ceil($panjang / $key);
                $panjang = $dividen * $key;
            }

            // menyusun matriks 
            for ($idx = 0; $idx < $panjang; $idx++) {
                if ($kolom >= ($dividen - 1) && $baris > 0) {
                    $matriks[$baris][$kolom] = 'ks';
                    $idx = $idx - 1;
                } else {
                    $matriks[$baris][$kolom] = isset($text[$idx])? $text[$idx] : 'ks';  
                }

                if ($kolom == $dividen - 1) {
                    $kolom = 0;
                    $baris = $baris + 1;
                } else { 
                    $kolom = $kolom + 1;
                }

                if ($baris > ($key - 1)){
                    break;
                }
            }
            
            // mereset baris dan kolom
            $baris = 0;
            $kolom = 0;

            // dd($matriks, $dividen - 1);

            // mentranspose matriks
            $hasil = '';
            for ($idx = 0; $idx < $panjang; $idx++) {
                if ($matriks[$baris][$kolom] == 'ks') {
                    $hasil = $hasil . '';
                } else {
                    $hasil = $hasil . $matriks[$baris][$kolom];
                }
                if($baris == ($key - 1)){
                    $kolom = $kolom + 1;
                    $baris = 0;
                } else {
                    $baris = $baris + 1;
                }
            }

            // dd($matriks, $hasil);

            return view('dashboard', [
                'hasil' => $hasil,
                'metode' => $metode,
                'key' => $key,
                'text' => $text
            ]);
        }

        //enkripsi substitusi
        elseif ($metode == '3'){
            $text = strtolower($text);
            $hasil = '';

            for ($i = 0; $i < strlen($text); $i++) {
                $char = $text[$i];

                if (ctype_alpha($char)) {
                    $offset = ord('a');
                    $encryptedChar = chr(($this->getCharIndex($char) + $key) % 26 + $offset);
                    $hasil .= ctype_upper($char) ? strtoupper($encryptedChar) : $encryptedChar;
                } else {
                    $hasil .= $char;
                }
            }
            
            return view('dashboard', [
                'hasil' => $hasil,
                'metode' => $metode,
                'key' => $key,
                'text' => $text
            ]);
        }
        
        //dekripsi substitusi
        else{
            $text = strtolower($text);
            $hasil = '';

            for ($i = 0; $i < strlen($text); $i++) {
                $char = $text[$i];

                if (ctype_alpha($char)) {
                    $offset = ord('a');
                    $decryptedChar = chr(($this->getCharIndex($char) - $key + 26) % 26 + $offset);
                    $hasil .= ctype_upper($char) ? strtoupper($decryptedChar) : $decryptedChar;
                } else {
                    $hasil .= $char;
                }
            }

            return view('dashboard', [
                'hasil' => $hasil,
                'metode' => $metode,
                'key' => $key,
                'text' => $text
            ]);
        }
    }

    private function getCharIndex($char)
    {
        return ord(strtolower($char)) - ord('a');
    }
}