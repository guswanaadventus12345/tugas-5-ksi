document
    .getElementById("metodeDropdown")
    .addEventListener("change", function () {
        var selectedValue = this.value;
        var labelTextInput = document.querySelector('label[for="textInput"]');
        var labelHasil = document.querySelector('label[for="hasil"]');

        // Update label text based on selected dropdown value
        switch (selectedValue) {
            case "1":
                labelTextInput.textContent = "Masukan Plain Text:";
                labelHasil.textContent = "Hasil (Chiper Text):";
                break;
            case "2":
                labelTextInput.textContent = "Masukan Chiper Text:";
                labelHasil.textContent = "Hasil (Plain Text):";
                break;
            case "3":
                labelTextInput.textContent = "Masukan Plain Text:";
                labelHasil.textContent = "Hasil (Chiper Text):";
                break;
            case "4":
                labelTextInput.textContent = "Masukan Chiper Text:";
                labelHasil.textContent = "Hasil (Plain Text):";
                break;
            default:
                labelTextInput.textContent = "Masukan Text:";
                labelHasil.textContent = "Hasil:";
                break;
        }
    });

function validateForm() {
    var textInput = document.getElementById("textInput").value;
    var keyInput = document.getElementById("keyInput").value;
    var metodeInput = document.getElementById("metodeDropdown").value;

    if (metodeInput === "-") {
        alert("Harap isi metode sebelum mengklik submit.");
        return false;
    }
    if (textInput === "-") {
        alert("Harap isi teks sebelum mengklik submit.");
        return false;
    }
    if (keyInput === "-") {
        alert("Harap isi key sebelum mengklik submit.");
        return false;
    }
    return true;
}
