<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="css/navbar.css">
    <link rel="stylesheet" href="css/content.css">
    <link rel="stylesheet" href="css/footer.css">

    <!-- Logo -->
    <link rel="icon" href="img/key.svg" type="image/svg">

    <title>Mesin Chiper Text</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg">

        <a class="navbar-brand ml-5" href="#"> 
            <img src="img/key.svg" alt="Mesin-Chiper"
            id="logo"> 
            Mesin-Chiper 
        </a>

    </nav>

    <section>
        <div class="container">
            <div>
                <div class="pb-5">
                    <h1 class="display-5">Selamat Datang!!</h1>
                    <p class="lead">
                        silahkan pilih mesin enkripsi atau dekripsi yang ingin digunakan..
                    </p>
                    <hr class="my-4">
                    <div class="d-flex justify-content-center">
                        <div class="card" style="width: 500px; height: 500px;">                            

                            <div class="card-body">
                                <form action="{{ route('proses') }}" method="POST" onsubmit="return validateForm()">
                                    @csrf
                                    <h5 class="card-title">Pilih Metode</h5>
                                    <select class="form-control" id="metodeDropdown" name="metode">
                                        <option value="-">Pilih Metode</option>
                                        <option value="1" {{ ($metode == 1)? 'selected' : '' }}>Transformation Encryption</option>
                                        <option value="2" {{ ($metode == 2)? 'selected' : '' }}>Transformation Decryption</option>
                                        <option value="3" {{ ($metode == 3)? 'selected' : '' }}>Substitution Encryption</option>
                                        <option value="4" {{ ($metode == 4)? 'selected' : '' }}>Substitution Decryption</option>
                                    </select>
                                    
                                    <label class="mt-4" for="textInput">Masukan Text: </label>
                                    <input type="text" class="form-control" id="textInput" value="{{ isset($text)? $text : '' }}" name="text" placeholder="Enter text">
                                    
                                    <label class="mt-4" for="keyInput">Pilih Kunci: </label>
                                    <select class="form-control" name="key" id="keyInput">
                                        <option value="-">Pilih Key</option>
                                        <option value="2" {{ ($key == 2)? 'selected' : '' }}>2</option>
                                        <option value="3" {{ ($key == 3)? 'selected' : '' }}>3</option>
                                        <option value="4" {{ ($key == 4)? 'selected' : '' }}>4</option>
                                        <option value="5" {{ ($key == 5)? 'selected' : '' }}>5</option>
                                        <option value="6" {{ ($key == 6)? 'selected' : '' }}>6</option>
                                        <option value="7" {{ ($key == 7)? 'selected' : '' }}>7</option>
                                        <option value="8" {{ ($key == 8)? 'selected' : '' }}>8</option>
                                        <option value="9" {{ ($key == 9)? 'selected' : '' }}>9</option>
                                        <option value="10" {{ ($key == 10)? 'selected' : '' }}>10</option> 
                                    </select>

                                    <button type="submit" id="submitBtn" class="btn btn-primary mt-4">Submit</button>
                                </form>

                                <hr class="my-3">
                                <label style="font-weight: 700" for="hasil">Hasil:</label>
                                <div>
                                    <input type="text" id="hasil" class="form-control" value="{{ isset($hasil)? $hasil : '' }}" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <footer>
        <div class="card">
            <div class="card-header pl-5">
                Quotes from developer
            </div>
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                    <p class="pl-4">
                        It is well and have fun with this app guys!!
                    </p>
                    <p class="pl-4">
                        by: Guswana Adventus
                    </p>
                </blockquote>
            </div>
          </div>
    </footer>

    <script src="js/metode.js"></script>

  </body>
</html>