<?php

use App\Http\Controllers\ProsesController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('dashboard',[
        'metode' => '0',
        'key' => '0'
    ]);
});

Route::post('/proses', [ProsesController::class, 'proses'])->name('proses');